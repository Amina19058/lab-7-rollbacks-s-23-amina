-- CREATE DATABASE lab;
-- \c lab
CREATE TABLE Player (
    username    TEXT    UNIQUE, 
    balance     INT     CHECK(balance >= 0)
);

CREATE TABLE Shop (
    product    TEXT    UNIQUE, 
    in_stock   INT     CHECK(in_stock >= 0), 
    price      INT     CHECK (price >= 0)
);

CREATE TABLE Inventory (
    username    TEXT    REFERENCES Player(username) NOT NULL,
    product     TEXT    REFERENCES Shop(product)    NOT NULL,
    amount      INT     CHECK(amount >= 0),
    UNIQUE(username, product)
);

INSERT INTO Player  (username, balance)
VALUES ('Alice', 100);
INSERT INTO Player  (username, balance)
VALUES ('Bob', 200);
INSERT INTO Shop    (product, in_stock, price)
VALUES ('marshmello', 10, 10);