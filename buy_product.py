import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"

get_inventory_total = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
update_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"
INVENTORY_LIMIT = 100


def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port="5432"
    )


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")
            try:
                cur.execute(get_inventory_total, obj)
                inventory_total = cur.fetchone()[0] or 0
                
                if inventory_total + amount > INVENTORY_LIMIT:
                    raise Exception("Inventory limit for products is reached")

                cur.execute(update_inventory, obj)
                if cur.rowcount != 1:
                    raise Exception("Failed to update inventory")
            except Exception as e:
                conn.rollback()
                raise e

buy_product('Alice', 'marshmello', 1)
